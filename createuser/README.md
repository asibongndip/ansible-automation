

[vagrant@ansiblecore createuser]$ ansible-playbook -i inventory createuser.yaml

PLAY [all] ***************************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************
ok: [node42.example.com]
ok: [node41.example.com]

TASK [Checking group name - group id is matching] ************************************************************************************************************
ok: [node41.example.com]
ok: [node42.example.com]

TASK [debug] *************************************************************************************************************************************************
ok: [node41.example.com] => {
    "groupcheck_result": {
        "changed": false, 
        "failed": false, 
        "meta": {
            "sysadmin": {
                "Description": "Group exist with the correct group id", 
                "Matching": true, 
                "groupid": 1001, 
                "groupname": "sysadmin"
            }
        }, 
        "validation": true
    }
}
ok: [node42.example.com] => {
    "groupcheck_result": {
        "changed": false, 
        "failed": false, 
        "meta": {
            "sysadmin": {
                "Description": "Group exist but group id is not matching", 
                "Matching": false, 
                "groupid": 1001, 
                "groupname": "sysadmin"
            }
        }, 
        "validation": false
    }
}

TASK [Checking user name - user id are free to provision] ****************************************************************************************************
skipping: [node42.example.com]
ok: [node41.example.com]

TASK [debug] *************************************************************************************************************************************************
ok: [node41.example.com] => {
    "usercheck_result": {
        "changed": false, 
        "failed": false, 
        "meta": {
            "ndip5": {
                "Create": true, 
                "Description": "User name and id are free to provision", 
                "userid": 1055, 
                "username": "ndip5"
            }, 
            "ndip6": {
                "Create": true, 
                "Description": "User name and id are free to provision", 
                "userid": 1056, 
                "username": "ndip6"
            }, 
            "ndip7": {
                "Create": true, 
                "Description": "User name and id are free to provision", 
                "userid": 1057, 
                "username": "ndip7"
            }, 
            "ndip8": {
                "Create": true, 
                "Description": "User name and id are free to provision", 
                "userid": 1058, 
                "username": "ndip8"
            }, 
            "ndip9": {
                "Create": true, 
                "Description": "User name and id are free to provision", 
                "userid": 1059, 
                "username": "ndip9"
            }
        }
    }
}
ok: [node42.example.com] => {
    "usercheck_result": {
        "changed": false, 
        "skip_reason": "Conditional result was False", 
        "skipped": true
    }
}

TASK [Creating users] ****************************************************************************************************************************************
[WARNING]: conditional statements should not include jinja2 templating delimiters such as {{ }} or {% %}. Found: usercheck_result.meta.{{ item.key }}.Create
skipping: [node42.example.com] => (item={'value': 1056, 'key': u'ndip6'}) 
skipping: [node42.example.com] => (item={'value': 1057, 'key': u'ndip7'}) 
skipping: [node42.example.com] => (item={'value': 1055, 'key': u'ndip5'}) 
skipping: [node42.example.com] => (item={'value': 1058, 'key': u'ndip8'}) 
skipping: [node42.example.com] => (item={'value': 1059, 'key': u'ndip9'}) 
changed: [node41.example.com] => (item={'value': 1056, 'key': u'ndip6'})
changed: [node41.example.com] => (item={'value': 1057, 'key': u'ndip7'})
changed: [node41.example.com] => (item={'value': 1055, 'key': u'ndip5'})
changed: [node41.example.com] => (item={'value': 1058, 'key': u'ndip8'})
changed: [node41.example.com] => (item={'value': 1059, 'key': u'ndip9'})

PLAY RECAP ***************************************************************************************************************************************************
node41.example.com         : ok=6    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
node42.example.com         : ok=4    changed=0    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0   

