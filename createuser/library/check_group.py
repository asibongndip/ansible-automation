#!/usr/bin/python

DOCUMENTATION = '''
---
module: check_group
short_description: check_group module will check for group names and default GIDs. Used for determining which GID to use if the groupname and GID do not exist on a specific server
'''

EXAMPLES = '''
- name: Checking group name - group id is matching
  check_group:
    groups: {"sysadmin":1003}
  register: groupcheck_result
'''


from ansible.module_utils.basic import *
import pwd, grp
import itertools

def checkgrpexist(data):
    checkgrp = {}
    groups = grp.getgrall()
    for group in groups:
        checkgrp[group[0]] = group[2]
    allgivengroup = {}
    grpvalidation = []
    groupdict = data['groups']
    for (groupname,groupid) in zip(groupdict.keys(),groupdict.values()):
        sysgrpkey,sysgrpvalue = groupname,int(groupid)
        if sysgrpkey in checkgrp and sysgrpvalue == checkgrp[sysgrpkey]:
            print("group sysadmin exist")
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Description": "Group exist with the correct group id", "Matching": True}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(True)
            #return False, False , meta
        elif sysgrpkey in checkgrp and sysgrpvalue != checkgrp[sysgrpkey]:
            print("group sysadmin exist")
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Description": "Group exist but group id is not matching", "Matching": False}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(False)
            #return False, False , meta
        else:
            print("group sysadmin dint exist")
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Matching": False}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(False)
            #return False, False , meta
    if False in set(grpvalidation):
        validation = False
    else:
        validation = True
    return False, False , allgivengroup, validation

def main():
    fields = {"groups": {"required": True, "type": "dict"},
              "state": {
        	"default": "present", 
        	"choices": ['present', 'absent'],  
        	"type": 'str' 
              },
    }
    choice_map = {
      "present": checkgrpexist,
    }
    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result, validation = choice_map.get(module.params['state'])(module.params)
    if not is_error:
        module.exit_json(changed=has_changed, meta=result, validation=validation)
    else:
        module.fail_json(msg="Provided group name and group id is not matching with the servers /etc/group", meta=result, validation=validation)
    

if __name__ == '__main__':
    main()
