#!/usr/bin/python

DOCUMENTATION = '''
---
module: check_user
short_description: check_user module will verify the user and userid was created correctly or not
'''

EXAMPLES = '''
- name: Checking user name - user id are free to provision
  check_user:
    users: {"ndip5":1055, "ndip6":1006, "ndip7":1007, "ndip8":1008, "ndip8":1008}
  register: usercheck_result
'''



from ansible.module_utils.basic import *
import pwd, grp
import itertools

def checkuserexist(data):
    checkuser = {}
    users = pwd.getpwall()
    for user in users:
        checkuser[user[0]] = user[2]
    allgivenuser = {}
    userdict = data['users']
    for (username,userid) in zip(userdict.keys(),userdict.values()):
        usernamekey,useridvalue = username,int(userid)
        if usernamekey in checkuser and useridvalue == checkuser[usernamekey]:
            print("User already created with respective correct userid")
            meta = {"username": usernamekey, "userid": useridvalue, "Create": False, "Description": "User already created with respective correct userid"}
            allgivenuser[username] = meta
            #return False, False , meta
        elif usernamekey in checkuser and useridvalue != checkuser[usernamekey]:
            print("User already created but with wrong userid")
            meta = {"username": usernamekey, "userid": useridvalue, "Create": False, "Description": "User already created but with wrong userid"}
            allgivenuser[username] = meta
            #return False, False , meta
        elif useridvalue in checkuser.values():
            print("Userid already used by different user")
            meta = {"username": usernamekey, "userid": useridvalue, "Create": False, "Description": "Userid already used by different user"} 
            allgivenuser[username] = meta
            #return False, False , meta
        else:
            print("User is not created")
            meta = {"username": usernamekey, "userid": useridvalue, "Create": True , "Description": "User name and id are free to provision"} 
            allgivenuser[username] = meta
            #return False, False , meta
    return False, False , allgivenuser

def main():
    fields = {"users": {"required": True, "type": "dict"},
              "state": {
        	"default": "present", 
        	"choices": ['present', 'absent'],  
        	"type": 'str' 
              },
    }
    choice_map = {
      "present": checkuserexist,
    }
    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result = choice_map.get(module.params['state'])(module.params)
    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="Provided user name or user id is already used on servers /etc/passwd", meta=result)
    

if __name__ == '__main__':
    main()
