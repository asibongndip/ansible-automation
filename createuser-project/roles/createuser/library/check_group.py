#!/usr/bin/python

DOCUMENTATION = '''
---
module: check_group
short_description: check_group module will check for group names and default GIDs. Used for determining which GID to use if the groupname and GID do not exist on a specific server
'''

EXAMPLES = '''
- name: Checking group name - group id is matching
  check_group:
    groups: {"sysadmin":1003}
  register: groupcheck_result
'''

import logging, platform, os, socket
from ansible.module_utils.basic import *
import pwd, grp
import itertools

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
file_handler = logging.FileHandler('ansible_change.log')
formatter    = logging.Formatter('%(asctime)s : %(levelname)s : %(message)s')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

def checkgrpexist(data):
    checkgrp = {}
    groups = grp.getgrall()
    for group in groups:
        checkgrp[group[0]] = group[2]
    allgivengroup = {}
    grpvalidation = []
    groupdict = {}
    for processgroupdict in data['groups']:
      groupdict.update({processgroupdict["gname"]:processgroupdict["gid"]})

    for (groupname,groupid) in zip(groupdict.keys(),groupdict.values()):
        sysgrpkey,sysgrpvalue = groupname,int(groupid)
        if sysgrpkey in checkgrp and sysgrpvalue == checkgrp[sysgrpkey]:
            print("Group exist with the correct group id")
            plog = "Group exist with the correct group id" + " --- Group Name: " + sysgrpkey + " Group ID: " + str(sysgrpvalue)
            logger.info(plog)
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Description": "Group exist with the correct group id", "validation": True}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(True)
            #return False, False , meta
        elif sysgrpkey in checkgrp and sysgrpvalue != checkgrp[sysgrpkey]:
            print("group sysadmin exist")
            plog = "Group exist but group id is not matching" + " --- Group Name: " + sysgrpkey + " Group ID: " + str(sysgrpvalue)
            logger.info(plog)
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Description": "Group exist but group id is not matching", "validation": False}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(False)
            #return False, False , meta
        elif sysgrpvalue in checkgrp.values():
            print("group sysadmin dint exist but gid already used by some other group")
            plog = "Group dint exist but group id is already used by someother group" + " --- Group Name: " + sysgrpkey + " Group ID: " + str(sysgrpvalue)
            logger.info(plog)
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Description": "Group dint exist but group id is already used by someother group", "validation": False}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(False)
        else:
            print("group sysadmin dint exist")
            plog = "group sysadmin dint exist and group id also available to create" + " --- Group Name: " + sysgrpkey + " Group ID: " + str(sysgrpvalue)
            logger.info(plog)
            meta = {"groupname": sysgrpkey, "groupid": sysgrpvalue, "Description": "Group and gid is dint exist - eligible to create group", "validation": True}
            allgivengroup[sysgrpkey] = meta
            grpvalidation.append(True)
            #return False, False , meta
    if False in set(grpvalidation):
        validation = False
    else:
        validation = True
    return False, False , allgivengroup, validation, groupdict

def main():
    fields = {"groups": {"required": True, "type": "list"},
              "state": {
        	"default": "present", 
        	"choices": ['present', 'absent'],  
        	"type": 'str' 
              },
    }
    choice_map = {
      "present": checkgrpexist,
    }
    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result, validation, groupdict = choice_map.get(module.params['state'])(module.params)
    if not is_error:
        module.exit_json(changed=has_changed, meta=result, validation=validation, groupdict=groupdict)
    else:
        module.fail_json(msg="Provided group name and group id is not matching with the servers /etc/group", meta=result, validation=validation, groupdict=groupdict)
    

if __name__ == '__main__':
    main()
